#include "ofApp.h"

void ofApp::setup(){
    ofBackground(0);
    ofSetFrameRate(30);
    fg = ofColor::cyan;
    path.setStrokeColor(fg);
    path.setStrokeWidth(2);
    path.setFilled(false);
    fbo.allocate(ofGetWidth(), ofGetHeight());
    ofSetCircleResolution(100);
}

void ofApp::update(){
}

void ofApp::draw(){

    int x = ofGetWidth() / 2;
    int y = ofGetHeight() / 2;
    int z = ofNoise(ofGetElapsedTimef()) * noiseAmount;
    fbo.begin();
    ofFill();
    ofSetColor(0, 0, 0, alpha * 255);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    ofNoFill();

    ofPushMatrix();
    ofTranslate(x, y, z); 
    ofRotateXDeg(rotationX);
    ofRotateYDeg(rotationY);

    path.clear();
    float radius_;
    ofPoint pos;
    for (int i=0; i<numPoints; i++){
        if(i % 2 == 0){
            radius_ = radius;
        } else { 
            radius_ = radius / ratio;
        };
        pos = polToCar(radius_, angle);
        float r = radius / 2;
        ofSetColor(fg);
        ofDrawCircle(pos.x, pos.y, r);
        path.lineTo(pos.x, pos.y);
        angle += 360 / numPoints;
    };
    path.close();

    path.draw();

    ofRotateZDeg(rotationZ);
    ofSetColor(fg);

    path.draw();

    ofDrawCircle(0, 0, radius);
    ofDrawCircle(0, 0, radius*1.5); 
    ofPopMatrix();

    fbo.end();
    fbo.draw(0, 0);

    rotationX += rotationXspeed;
    rotationY += rotationYspeed;
    rotationZ += rotationZspeed; 
}

ofPoint ofApp::polToCar(float r, float a){
    float theta = ofDegToRad(a);
    ofPoint car;
    car.x = r * cos(theta);
    car.y = r * sin(theta);
    return car;
}

void ofApp::keyPressed(int key){
    switch(key){
        case OF_KEY_UP:
            alpha += 0.05;
            alpha = ofClamp(alpha, 0.0, 1.0);
            break;

        case OF_KEY_DOWN:
            alpha -= 0.05;
            alpha = ofClamp(alpha, 0.0, 1.0);
            break; 

        case OF_KEY_LEFT:
            numPoints -= 2;
            numPoints = ofClamp(numPoints, 6, 36);
            break; 

        case OF_KEY_RIGHT:
            numPoints += 2;
            numPoints = ofClamp(numPoints, 6, 36);
            break; 
        
        case 'e':
            ratio += 0.1;
            ratio = ofClamp(ratio, 0.01, 10);
            break;

        case 'd':
            ratio -= 0.1;
            ratio = ofClamp(ratio, 0.01, 10);
            break;

        case 'r':
            radius += 0.5;
            radius = ofClamp(radius, 0.01, 10000);
            break;

        case 'f': 
            radius -= 0.5;
            radius = ofClamp(radius, 0.01, 10000);
            break; 
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
