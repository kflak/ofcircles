#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        ofPoint polToCar (float r, float a);
        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        ofPath path;
        float angle = 0;
        float radius = 300;
        float ratio = 4;
        float rotationX = 0;
        float rotationY = 0;
        float rotationZ = 0;
        float rotationXspeed = 1;
        float rotationYspeed = 1;
        float rotationZspeed = 2;
        float angleSpeed = 1;
        int numPoints = 6;
        float alpha = 0.5;
        float noiseAmount = 100;
        ofColor fg;

        ofFbo fbo;
};
